/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
public class TestBookBank {
    public static void main(String[] args) {
        BookBank book1 = new BookBank("Jane",6000.75);
        BookBank book2 = new BookBank("Harry",10000.05);
        
    /*  book1.name = "Jane";
        book1.money = 6000.75;
        book2.name = "Harry";
        book2.money = 10000.05;
     */   
        book1.deposit(1000.00);
        System.out.println(book1.name +" "+ book1.money);
        book1.withdraw(200.00);
        System.out.println(book1.name +" "+ book1.money);
        book2.withdraw(50000.55);
        System.out.println(book2.name +" "+ book2.money);
        book2.deposit(100.00);
        System.out.println(book2.name +" "+ book2.money);
        
        book2.withdraw(-100);
        System.out.println(book2.name +" "+ book2.money);
    }
    
}
