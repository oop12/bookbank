/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
public class BookBank {
    String name;
    double money;
    
    BookBank(String name,double money){
        this.name = name;
        this.money = money;
    }
    
    void deposit(double money){
        if(money <= 0){
            System.out.println("Money > 0!!");
            return;
        }
        this.money += money; 
    }
    void withdraw(double money){
        if(money <= 0){
            System.out.println("Money > 0!!");
            return;
        }
        if(money <= this.money){
            this.money -= money;
        }else{
            System.out.println("Not enough money");
        }
    }
}
